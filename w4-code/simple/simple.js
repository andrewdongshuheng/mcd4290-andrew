function doIt(){
    let nmu1Ref=document.getElementById("number1")
    let nmu2Ref=document.getElementById("number2")
    let nmu3Ref=document.getElementById("number3")
    let answerRef=document.getElementById("answer")
    let oddevenRef=document.getElementById("oddeven")
    answer=Number(nmu1Ref.value)+Number(nmu2Ref.value)+Number(nmu3Ref.value)
    answerRef.innerText=answer
    if(answer>=0){
        answerRef.className="positive"
        
    }
    else{
        answerRef.className="negative"
        
    }
    if(answer%2==0){
        oddevenRef.innerHTML="(even)"
        oddevenRef.className="even"
    }
    else{
        oddevenRef.innerHTML="(odd)"
        oddevenRef.className="odd"
    }
}