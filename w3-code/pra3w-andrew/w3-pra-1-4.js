//task 1
function objToHTML(obj){
    let str =""
    
    for(let prop in obj){
        str+= prop +" : "+ obj[prop]+"\n"
    }
    return str
}
var testObj = {
 number: 1,
 string: "abc",
 array: [5, 4, 3, 2, 1],
 boolean: true
};
let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = objToHTML(testObj) //this line will fill the above element with your output.

//task 2
var outputAreaRef = document.getElementById("outputArea2");
var output = "";
function flexible(fOperation, operand1, operand2)
{
 var result = fOperation(operand1, operand2);
 return result;
}
function plus(a,b){
    return a+b
}
function time(a,b){
    return a*b
}
output += flexible (plus,3,5) + "<br/>";
output += flexible (time,3,5) + "<br/>";
let outPutArea2 = document.getElementById("outputArea2")
    outPutArea2.innerHTML = output;

//task 3
/*
first we need creat a function and named as extremeValues()
for min, if second is smaller than first element then set second is min then follew this step until loop is to end element
for max, if second is larger than first element then set second is max then follew this step until loop is to end element
define an array called result=[min,max]
*/

//task 4
var values = [4, 3, 6, 12, 1, 3, 8];
extremeValues(values)
  function extremeValues(Array){
      let min=Array[0]
      let max=Array[0]
      for(let i=0;i<Array.length;i++){
          if (Array[i]<min){
              min=Array[i]
          }
          if (Array[i]>max){
              max=Array[i]
          }
      }
      let result=[min,max]
      let outPutArea3 = document.getElementById("outputArea3")
    outputArea3.innerText += result[1]+"\n"
    outputArea3.innerText += result[0]  
  }